package com.ultimatecrazy.progressbar_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ultimatecrazy.ultimateprogressbar.Interfaces.OnProgressUpdateListener
import com.ultimatecrazy.ultimateprogressbar.ProgressBar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressBar.setOnProgressUpdateListener(object : OnProgressUpdateListener {
            override fun onProgressUpdate(progress: Float) {

            }
        })


        button.setOnClickListener {

        }
    }
}
