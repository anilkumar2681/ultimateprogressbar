@file:Suppress("unused")
package com.ultimatecrazy.ultimateprogressbar.Utill

import android.content.Context
import android.graphics.Typeface
import androidx.core.content.ContextCompat
import com.ultimatecrazy.ultimateprogressbar.R

@DslMarker
annotation class TextFormatterDsl


/** create an instance of [TextFormatter] from [TextFormatter.Builder] using kotlin dsl. */
fun textFormatter(context: Context, block: TextFormatter.Builder.() -> Unit): TextFormatter =
    TextFormatter.Builder(context).apply(block).build()


class TextFormatter(builder:Builder) {

    val text = builder.text
    val textSize = builder.textSize
    val textColor = builder.textColor
    val textStyle = builder.textTypeface
    val textStyleObject = builder.textTypefaceObject


    /** Builder class for [TextFormatter]. */
    @TextFormatterDsl
    class Builder(context: Context) {

        @JvmField
        var text:String?=""
        @JvmField
        var textSize: Float = 12f

        @JvmField
        var textColor = ContextCompat.getColor(context, R.color.white)
        @JvmField
        var textTypeface = Typeface.NORMAL
        @JvmField
        var textTypefaceObject: Typeface? = null

        fun setText(value: String): Builder = apply { this.text = value }
        fun setTextSize(value: Float): Builder = apply { this.textSize = value }
        fun setTextColor(value: Int): Builder = apply { this.textColor = value }
        fun setTextTypeface(value: Int): Builder = apply { this.textTypeface = value }
        fun setTextTypeface(value: Typeface): Builder = apply { this.textTypefaceObject = value }
        fun build(): TextFormatter {
            return TextFormatter(this)
        }

    }
}