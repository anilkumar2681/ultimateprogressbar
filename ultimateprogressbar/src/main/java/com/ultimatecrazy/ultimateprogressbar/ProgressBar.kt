package com.ultimatecrazy.ultimateprogressbar

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.ultimatecrazy.ultimateprogressbar.Enums.UltimateProgressBarOrientation
import com.ultimatecrazy.ultimateprogressbar.Extensions.*
import com.ultimatecrazy.ultimateprogressbar.Extensions.compactColor
import com.ultimatecrazy.ultimateprogressbar.Extensions.dpToPx
import com.ultimatecrazy.ultimateprogressbar.Extensions.pxToSp
import com.ultimatecrazy.ultimateprogressbar.Interfaces.OnProgressUpdateListener
import com.ultimatecrazy.ultimateprogressbar.Utill.TextFormatter
import com.ultimatecrazy.ultimateprogressbar.Utill.textFormatter


@DslMarker
annotation class UltimateProgressBarDSL

class ProgressBar : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet, defStyle: Int) : super(
        context,
        attributeSet, defStyle
    ) {
        getAttrs(attributeSet, defStyle)
    }

    val labelView = TextView(context)
    var duration = 1000L
    var autoAnimation = true
    private var onProgressUpdateListener: OnProgressUpdateListener? = null
    private val path = Path()
    var min = 0f
    var max = 100f
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var progress = 0f
        set(value) {
            field = when {
                value >= max -> max
                value <= min -> min
                else -> value
            }
            updateUltimateProgressBar()
            onProgressUpdateListener?.onProgressUpdate(field)
        }

    var orientation = UltimateProgressBarOrientation.HORIZONTAL
        set(value) {
            field = value
            updateUltimateProgressBar()
        }

    var background_Color = compactColor(R.color.white)
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var radius = dpToPx(5).toFloat()
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var labelText: String? = ""
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var labelSize = 12f
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var labelColorInner = compactColor(R.color.white)
        set(value) {
            field = value
            updateUltimateProgressBar()
        }

    var labelColorOuter = compactColor(R.color.black)
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var labelTypeFace = Typeface.NORMAL
        set(value) {
            field = value
            updateUltimateProgressBar()
        }

    var labelTypefaceObject: Typeface? = null
        set(value) {
            field = value
            updateUltimateProgressBar()
        }
    var labelSpace = dpToPx(8).toFloat()
        set(value) {
            field = value
            updateUltimateProgressBar()
        }

    private fun getAttrs(attributeSet: AttributeSet, defStyleAttr: Int) {
        val typedArray =
            context.obtainStyledAttributes(attributeSet, R.styleable.ProgressBar, defStyleAttr, 0)
        try {
            setTypeArray(typedArray)
        } finally {
            typedArray.recycle()
        }
    }

    private fun setTypeArray(typedArray: TypedArray) {

        this.labelText = typedArray.getString(R.styleable.ProgressBar_progressbar_labelText)
        this.labelSize = pxToSp(
            typedArray.getDimension(
                R.styleable.ProgressBar_progressbar_labelSize,
                labelSize
            )
        )
        this.labelSpace =
            typedArray.getDimension(R.styleable.ProgressBar_progressbar_labelSpace, labelSpace)
        this.labelColorInner = typedArray.getColor(
            R.styleable.ProgressBar_progressbar_labelColorInner,
            labelColorInner
        )
        this.labelColorOuter = typedArray.getColor(
            R.styleable.ProgressBar_progressbar_labelColorOuter,
            labelColorOuter
        )

        when (typedArray.getInt(
            R.styleable.ProgressBar_progressbar_labelTypeface,
            Typeface.NORMAL
        )) {
            0 -> this.labelTypeFace = Typeface.NORMAL
            1 -> this.labelTypeFace = Typeface.BOLD
            2 -> this.labelTypeFace = Typeface.ITALIC
        }

        when (typedArray.getInt(
            R.styleable.ProgressBar_progressbar_orientation,
            UltimateProgressBarOrientation.HORIZONTAL.value
        )) {
            0 -> this.orientation = UltimateProgressBarOrientation.HORIZONTAL
            1 -> this.orientation = UltimateProgressBarOrientation.VERTICAL
        }

        this.min = typedArray.getFloat(R.styleable.ProgressBar_progressbar_min, min)
        this.max = typedArray.getFloat(R.styleable.ProgressBar_progressbar_max, max)
        this.progress = typedArray.getFloat(R.styleable.ProgressBar_progressbar_progress, progress)
        this.radius = typedArray.getDimension(R.styleable.ProgressBar_progressbar_radius, radius)
        this.duration =
            typedArray.getInteger(R.styleable.ProgressBar_progressbar_duration, duration.toInt())
                .toLong()
        this.background_Color =
            typedArray.getColor(R.styleable.ProgressBar_progressbar_background_Color, background_Color)
        this.autoAnimation =
            typedArray.getBoolean(R.styleable.ProgressBar_progressbar_autoAnimation, autoAnimation)

    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        updateUltimateProgressBar()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        this.path.apply {
            reset()
            addRoundRect(
                RectF(0f, 0f, w.toFloat(), h.toFloat()),
                floatArrayOf(radius, radius, radius, radius, radius, radius, radius, radius),
                Path.Direction.CCW
            )
        }
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        canvas.clipPath(this.path)
    }


    private fun updateUltimateProgressBar() {
        post {
            updateLabel()
        }
        updateBackground()
        updateOrientation()
        autoAnimate()

    }


    @SuppressLint("NewApi")
    private fun updateBackground() {
        val drawable = GradientDrawable()
        drawable.cornerRadius = radius
        drawable.setColor(background_Color)
        this.background = drawable
    }

    private fun updateOrientation() {
        if (orientation == UltimateProgressBarOrientation.VERTICAL) {
            rotation = 180f
            labelView.rotation = 180f
        }
    }

    private fun autoAnimate() {
        if (this.autoAnimation) {
            progressAnimate()
        }
    }

    private fun updateLabel() {

        var params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )

        if (!isVertical()) {
            this.labelView.gravity = Gravity.CENTER_VERTICAL
        } else {
            params = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            this.labelView.gravity = Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL
        }

        this.labelView.layoutParams = params
        applyTextForm(textFormatter(context) {
            text = labelText
            textSize = labelSize
            textTypeface = labelTypeFace
            textTypefaceObject = labelTypefaceObject
        })
        removeView(labelView)
        addView(labelView)

        post {
            when {
                this.labelView.width + labelSpace < getProgressSize() -> {
                    setLabelViewPosition(getProgressSize() - this.labelView.width - this.labelSpace)
                    this.labelView.setTextColor(labelColorInner)
                }
                else -> {
                    setLabelViewPosition(getProgressSize() + this.labelSpace)
                    this.labelView.setTextColor(labelColorOuter)
                }
            }
        }


    }


    private fun isVertical(): Boolean {
        return orientation == UltimateProgressBarOrientation.VERTICAL
    }


    /** animates [ProgressBar]'s progress. */
    fun progressAnimate() {
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = this@ProgressBar.duration
            addUpdateListener {
                val value = it.animatedValue as Float
                setLabelViewPosition(getLabelPosition() * value)
//                highlightView.updateLayoutParams {
//                    if (isVertical()) {
//                        height = (getProgressSize() * value).toInt()
//                    } else {
//                        width = (getProgressSize() * value).toInt()
//                    }
//                }
            }
            start()
        }
    }

    private fun setLabelViewPosition(position: Float) {
        if (isVertical()) {
            labelView.y = position
        } else {
            labelView.x = position
        }
    }

    private fun getLabelPosition(): Float {
        return when {
            this.labelView.width + labelSpace < getProgressSize() -> getProgressSize() - this.labelView.width - this.labelSpace
            else -> getProgressSize() + this.labelSpace
        }
    }


    private fun getProgressSize(): Float {
        return (getViewSize(this) / max) * progress
    }

    private fun getViewSize(view: View): Int {
        return if (isVertical()) view.height
        else view.width
    }

    /** set a progress update listener. */
    fun setOnProgressUpdateListener(onProgressUpdateListener: OnProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener
    }

//    /** set a progress update listener. */
//    fun setOnProgressChangeListener(block: (Float) -> Unit) {
//        this.onProgressUpdateListener = object : OnProgressUpdateListener {
//            override fun onProgressUpdate(progress: Float) {
//                block(progress)
//            }
//        }
//    }

    fun applyTextForm(textFormatter: TextFormatter) {
        this.labelView.applyTextFormatter(textFormatter)
    }

    /** Builder class for creating [UltimateProgressBar]. */
    @UltimateProgressBarDSL
    class Builder(context: Context) {
        private val UltimateProgressBar = ProgressBar(context)

        fun setSize(width: Int, height: Int): Builder = apply {
            this.UltimateProgressBar.layoutParams =
                LayoutParams(UltimateProgressBar.dpToPx(width), UltimateProgressBar.dpToPx(height))
        }

        fun setHeight(value: Int): Builder = apply { this.UltimateProgressBar.layoutParams.height = value }
        fun setDuration(value: Long): Builder = apply { this.UltimateProgressBar.duration = value }
        fun setAutoAnimate(value: Boolean): Builder = apply { this.UltimateProgressBar.autoAnimation = value }
        fun setMin(value: Float): Builder = apply { this.UltimateProgressBar.min = value }
        fun setMax(value: Float): Builder = apply { this.UltimateProgressBar.max = value }
        fun setProgress(value: Float): Builder = apply { this.UltimateProgressBar.progress = value }
        fun setOrientation(value: UltimateProgressBarOrientation): Builder = apply {
            this.UltimateProgressBar.orientation = value
        }

        fun setColorBackground(value: Int): Builder = apply {
            this.UltimateProgressBar.background_Color = value
        }

        fun setRadius(value: Float): Builder = apply { this.UltimateProgressBar.radius = value }
        fun setLabelText(value: String): Builder = apply { this.UltimateProgressBar.labelText = value }
        fun setLabelSize(value: Float): Builder = apply {
            this.UltimateProgressBar.labelSize = this.UltimateProgressBar.spToPx(value)
        }

        fun setLabelSpace(value: Float): Builder = apply { this.UltimateProgressBar.labelSpace = value }
        fun setLabelColorInner(value: Int): Builder = apply {
            this.UltimateProgressBar.labelColorInner = value
        }

        fun setLabelColorOuter(value: Int): Builder = apply {
            this.UltimateProgressBar.labelColorOuter = value
        }

        fun setLabelTypeface(value: Int): Builder = apply { this.UltimateProgressBar.labelTypeFace = value }
        fun setLabelTypeface(value: Typeface): Builder = apply {
            this.UltimateProgressBar.labelTypefaceObject = value
        }

//        fun setUltimateProgressBarAlpha(value: Float): Builder = apply {
//            this.UltimateProgressBar.highlightView.alpha = value
//        }
//
//        fun setUltimateProgressBarColor(value: Int): Builder = apply {
//            this.progressView.highlightView.color = value
//        }
//
//        fun setUltimateProgressBarColorGradientStart(value: Int): Builder = apply {
//            this.progressView.highlightView.colorGradientStart = value
//        }

//        fun setUltimateProgressBarColorGradientEnd(value: Int): Builder = apply {
//            this.progressView.highlightView.colorGradientEnd = value
//        }
//
//        fun setUltimateProgressBarRadius(value: Float): Builder = apply {
//            this.progressView.highlightView.radius = value
//        }
//
//        fun setUltimateProgressBarPadding(value: Float): Builder = apply {
//            this.progressView.highlightView.padding = value
//        }
//
//        fun setHighlightColor(value: Int): Builder = apply {
//            this.progressView.highlightView.highlightColor = value
//        }
//
//        fun setHighlighting(value: Boolean): Builder = apply {
//            this.progressView.highlightView.highlighting = value
//        }
//
//        fun setHighlightThickness(value: Int): Builder = apply {
//            this.progressView.highlightView.highlightThickness = value
//        }

        fun setOnProgressUpdateListener(value: OnProgressUpdateListener): Builder = apply {
            this.UltimateProgressBar.onProgressUpdateListener = value
        }

//        fun setOnProgressClickListener(value: OnProgressClickListener): Builder = apply {
//            this.progressView.onProgressClickListener = value
//        }

        fun setTextFormatter(value: TextFormatter): Builder = apply {
            this.UltimateProgressBar.labelView.applyTextFormatter(value)
        }

        fun setOnProgressUpdateListener(block: (Float) -> Unit): Builder = apply {
            this.UltimateProgressBar.onProgressUpdateListener = object : OnProgressUpdateListener {
                override fun onProgressUpdate(progress: Float) {
                    block(progress)
                }

            }
        }

//        fun setOnProgressClickListener(block: (Boolean) -> Unit): Builder = apply {
//            this.progressView.onProgressClickListener = object : OnProgressClickListener {
//                override fun onClickProgress(highlighting: Boolean) {
//                    block(highlighting)
//                }
//            }
//        }

        fun build() = UltimateProgressBar
    }

}