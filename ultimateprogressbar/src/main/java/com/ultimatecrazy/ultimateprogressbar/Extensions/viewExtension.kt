@file:Suppress("UNCHECKED_CAST")

package com.ultimatecrazy.ultimateprogressbar.Extensions

import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat

/** dp size to px size. */

internal fun View.dpToPx(dp: Int): Int {
    val scale = resources.displayMetrics.density
    return (dp * scale).toInt()
}

/** sp size to px size. */

internal fun View.spToPx(sp: Float): Float {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.displayMetrics)
}

/** px size to sp size. */

internal fun View.pxToSp(px: Float): Float {
    return px / resources.displayMetrics.scaledDensity
}

/** gets color from the ContextCompat. */

internal fun View.compactColor(color:Int):Int{
    return ContextCompat.getColor(context, color)
}


/** update [FrameLayout] params. */

public fun ViewGroup.updateLayoutParams(block: ViewGroup.LayoutParams.() -> Unit) {
    layoutParams?.let {
        val params: ViewGroup.LayoutParams =
            (layoutParams as ViewGroup.LayoutParams).apply { block(this) }
        layoutParams = params
    }
}