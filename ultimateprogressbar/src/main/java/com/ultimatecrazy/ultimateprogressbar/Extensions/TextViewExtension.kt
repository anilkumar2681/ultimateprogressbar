package com.ultimatecrazy.ultimateprogressbar.Extensions

import android.util.TypedValue
import android.widget.TextView
import com.ultimatecrazy.ultimateprogressbar.Utill.TextFormatter


/** applies text form attributes to a TextView instance. */

internal fun TextView.applyTextFormatter(textFormatter: TextFormatter){

    text = textFormatter.text
    setTextSize(TypedValue.COMPLEX_UNIT_SP, textFormatter.textSize)
    setTextColor(textFormatter.textColor)
    textFormatter.textStyleObject?.let {
        typeface = textFormatter.textStyleObject
    } ?: setTypeface(typeface, textFormatter.textStyle)

}