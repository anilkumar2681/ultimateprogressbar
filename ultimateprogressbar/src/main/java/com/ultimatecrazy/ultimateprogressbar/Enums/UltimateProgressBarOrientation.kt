package com.ultimatecrazy.ultimateprogressbar.Enums


/** ProgressViewOrientation is the orientation attribute. */
@Suppress("unused")
enum class UltimateProgressBarOrientation(val value:Int) {

    HORIZONTAL(0),
    VERTICAL(1)
}