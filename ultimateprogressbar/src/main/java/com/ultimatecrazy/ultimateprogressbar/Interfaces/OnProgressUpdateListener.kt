package com.ultimatecrazy.ultimateprogressbar.Interfaces

/**
 * OnProgressUpdateListener is an interface for listening to the progress update.
 */
interface OnProgressUpdateListener {

    /** called when the progress is updated */

    fun onProgressUpdate(progress:Float)
}